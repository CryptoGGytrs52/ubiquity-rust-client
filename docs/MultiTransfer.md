# MultiTransfer

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**inputs** | [**Vec<crate::models::Utxo>**](utxo.md) |  | 
**outputs** | [**Vec<crate::models::Utxo>**](utxo.md) |  | 
**currency** | [**crate::models::Currency**](currency.md) |  | 
**total_in** | **String** | Integer string in smallest unit (Satoshis) | 
**total_out** | **String** | Integer string in smallest unit (Satoshis) | 
**unspent** | **String** | Integer string in smallest unit (Satoshis) | 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


