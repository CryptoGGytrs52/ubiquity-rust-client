# ReportFieldMeta

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**_type** | Option<**String**> |  | [optional][default to algorand_meta]
**sender_reward** | Option<**String**> |  | [optional]
**recipient_reward** | Option<**String**> |  | [optional]
**close** | Option<**String**> |  | [optional]
**close_amount** | Option<**String**> |  | [optional]
**close_reward** | Option<**String**> |  | [optional]

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


